#! /usr/bin/env python3
import sys

no = int(next(sys.stdin))-1

kr =[]
kr.append("                    .-._   _ _ _ _ _ _ _ _        ") 
kr.append("         .-''-.__.-'00  '-' ' ' ' ' ' ' ' '-.     ") 
kr.append("         '.___ '    .   .--_'-' '-' '-' _'-' '._  ") 
kr.append("          V: V 'vv-'   '_   '.       .'  _..' '.'.") 
kr.append("            '=.____.=_.--'   :_._._._:_   '.   : :") 
kr.append("                    (((____.-'        '-.  /   : :") 
kr.append("          snd                         (((-'\ .' / ") 
kr.append("                                    _____..'  .'  ") 
kr.append("                                   '-._____.-'    ")

kr[0] = kr[0][:31] + no*"_ _ " + kr[0][31:]
kr[1] = kr[1][:31] + no*" ' '" + kr[1][31:]
kr[2] = kr[2][:31] + no*" '-'" + kr[2][31:]
kr[3] = kr[3][:31] + no*"    " + kr[3][31:]
kr[4] = kr[4][:31] + no*"._._" + kr[4][31:]
kr[5] = kr[5][:31] + no*"    " + kr[5][31:]
kr[6] = kr[6][:31] + no*"    " + kr[6][31:]
kr[7] = kr[7][:31] + no*"    " + kr[7][31:]
kr[8] = kr[8][:31] + no*"    " + kr[8][31:]

for a in kr:
	print(a)