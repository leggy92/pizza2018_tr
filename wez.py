#! /usr/bin/env python3
import sys

word = next(sys.stdin)


error = sys.maxsize;
best = 0;


for k in range(128):
	x=k;
	current_error = 0;
	for i in word:
		x = (45*x + 1)%128;
		current_error = current_error + (x-ord(i))**2;
	if(current_error < error):
		error = current_error
		best = k	
print(best)