#!/bin/bash
path=$(pwd)

result=${PWD##*/}   
begin=00
end=10
extension=""
outputExtension=".out"
task=$result
inputDir=$path
outputDir="output"
while test $# -gt 0; do
        case "$1" in
                -h|--help)
                        echo "TRULULULZ PO CO CI HELP"
                        exit 0;
                        ;;
                -b|--begin)
                        shift
                        if test $# -gt 0; then
                                begin=$1
                        else
                                echo "no process specified"
                                exit 1
                        fi
                        shift
                        ;;
                -e|--end)
                        shift
                        if test $# -gt 0; then
                                end=$1
                        else
                                echo "no process specified"
                                exit 1
                        fi
                        shift
                        ;;
                -l|--extension|--language)
                        shift
                        if test $# -gt 0; then
                                extension=.$1
                        else
                                echo "no process specified"
                                exit 1
                        fi
                        shift
                        ;;      
                --outputExtension)
                        shift
                        if test $# -gt 0; then
                                outputExtension=.$1
                        else
                                echo "no process specified"
                                exit 1
                        fi
                        shift
                        ;;      
                -t|--task)
                        shift
                        if test $# -gt 0; then
                                task=$1
                        else
                                echo "no process specified"
                                exit 1
                        fi
                        shift
                        ;;
                -i|--input)
                        shift
                        if test $# -gt 0; then
                                inputDir="$path""/""$1"
                        else
                                echo "no process specified"
                                exit 1
                        fi
                        shift
                        ;;
                -o|--output)
                        shift
                        if test $# -gt 0; then
                                outputDir="$path""/""$1"
                        else
                                echo "no process specified"
                                exit 1
                        fi
                        shift
                        ;;

                *)
                        break
                        ;;
        esac
done
if [ ! -d "./$outputDir" ]; then
    mkdir $outputDir;
fi


for i in $(eval echo "{$begin..$end}")
do
	eval "./"$task""$extension" < "$inputDir"/"$task$i".in > "$outputDir""/""$task$i""$outputExtension""
done;

eval "zip -j '$task'.zip '$outputDir'/*"

